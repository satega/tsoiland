# This code seems to be in public domain
# http://www.google.com/search?client=opera&rls=en&q=unix+daemon+double+fork&sourceid=opera&ie=utf-8&oe=utf-8

import sys, os 

def daemonize():

    # do the UNIX double-fork magic, see Stevens' "Advanced 
    # Programming in the UNIX Environment" for details (ISBN 0201563177)
	try: 
		pid = os.fork() 
		if pid > 0:
			# exit first parent
			sys.exit(0) 
	except OSError, e: 
		print >>sys.stderr, "fork #1 failed: %d (%s)" % (e.errno, e.strerror) 
		sys.exit(1)

	# decouple from parent environment
	os.chdir("/") 
	os.setsid() 
	os.umask(0) 

	# do second fork
	try: 
		pid = os.fork() 
		if pid > 0:
			# exit from second parent, print eventual PID before
			print "Daemon PID %d" % pid 
			sys.exit(0) 
	except OSError, e: 
		print >>sys.stderr, "fork #2 failed: %d (%s)" % (e.errno, e.strerror) 
		sys.exit(1) 

	#TODO - move standard output to log file/library
	#	   	-there should be more here - see comments on http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/66012
	# Redirect standard file descriptors
	sys.stdin = open('/dev/null', 'r')
	sys.stdout = open('/dev/null', 'w')
	sys.stderr = open('/dev/null', 'w')
