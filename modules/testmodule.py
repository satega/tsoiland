# -*- coding: utf-8; -*-
#
# Copyright (C) 2008
# Torbjørn Søiland <tosoil@start.no>
# Knut Saua Mathiesen <ks.mathiesen@gmail.com>
#
# This file is part of Satega. 
#
# Satega is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Satega is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Satega.  If not, see <http://www.gnu.org/licenses/>.
#

class TestModule(object):
	def __init__(self, reg):
		self.someVariable = 'Hello'
		self.reg = reg
	def about(self):
		return "This module is a testmodule used during initial communication framework development."
		
	def someMethod(self):
		return self.someVariable
	
	def setSomeVariable(self, data):
		self.someVariable = data
		
	def testreg(self):
		return self.reg.getlib("test").echo("testreg")
