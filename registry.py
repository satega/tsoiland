# -*- coding: utf-8; -*-
#
# Copyright (C) 2008
# Torbjørn Søiland <tosoil@start.no>
#
# This file is part of Satega. 
#
# Satega is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Satega is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Satega.  If not, see <http://www.gnu.org/licenses/>.
#

from lib import test_lib
from lib import initd

class registry :
	"""An object of this class will be passed around to all the modules.
		It will contain references to library objects."""
	# TODO - make singleton
	def __init__(self):
		self.__reg = {}
		self.__reg["test"] = test_lib
		self.__reg["initd"] = initd
				
	def getlib(self, libname):
		return self.__reg[libname]
	
	
	
# Obviously just for testing	
if __name__ == "__main__":
	reg = registry()
	apt = reg.getlib("test")
	print apt
	print	apt.echo("hello")
	apt = reg.getlib("test")
