# -*- coding: utf-8; -*-
#
# Copyright (C) 2008
# Torbjørn Søiland <tosoil@start.no>
#
# This file is part of Satega. 
#
# Satega is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Satega is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Satega.  If not, see <http://www.gnu.org/licenses/>.
#
import subprocess


def start(servicename):
	"""Start service."""
	proc = subprocess.Popen('/etc/init.d/' + servicename + ' start', shell=True, stdout=subprocess.PIPE)
	stdout_value = proc.communicate()[0]
	return stdout_value
	
def stop(servicename):
	"""Stop service."""
	proc = subprocess.Popen('/etc/init.d/' + servicename + ' stop', shell=True, stdout=subprocess.PIPE)
	stdout_value = proc.communicate()[0]
	return stdout_value
	
def restart(servicename):
	"""Restart service."""
	proc = subprocess.Popen('/etc/init.d/' + servicename + ' restart', shell=True, stdout=subprocess.PIPE)
	stdout_value = proc.communicate()[0]
	return stdout_value


def autostart(servicename,auto):
	""" This function creates or deletes /etc/rcn.d/ scripts."""
	if auto:
		# Add startup scripts
		command = 'update-rc.d ' + servicename + ' defaults'
	else :	
		# Remove startupscripts even if /etc/init.d/servicename exists.
		command = 'update-rc.d -f ' + servicename + ' remove'
		print string
		
	# Run the command in a shell
	proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
	stdout_value = proc.communicate()[0]
	return stdout_value
	
if __name__ == "__main__":
	print start("bluetooth")
	print stop("bluetooth")
	print autostart("bluetooth", True)
